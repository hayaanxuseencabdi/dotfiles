-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

config.exit_behavior = 'Close'
config.adjust_window_size_when_changing_font_size = false
config.color_scheme = 'Catppuccin Frappe'
config.font = wezterm.font 'Liga SFMono Nerd Font'
config.font_size = 15
config.hide_tab_bar_if_only_one_tab = true
config.window_padding = {
  left = 0,
  right = 0,
  top = 0,
  bottom = 0,
}

-- Shortcuts
config.keys = {
  {
    key = 't',
    mods = 'CTRL|SHIFT',
    action = wezterm.action.SpawnTab 'CurrentPaneDomain',
  },
  -- {
  --   key = 'Enter',
  --   mods = 'CTRL|SHIFT',
  --   action = wezterm.action.SplitVertical {
  --     domain = 'CurrentPaneDomain',
  --   },
  -- },
}

return config
