-- :help options

local options = {
    clipboard = "unnamedplus",                  -- allows neovim to access the system clipboard
    fileencoding = "utf-8",                     -- the encoding written to a file
    hlsearch = true,                            -- highlight all matches on previous search pattern
    showtabline = 4,                            -- always show tabs
    smartcase = true,                           -- smart case
    smartindent = true,                         -- make indenting smarter again
    undofile = true,                            -- enable persistent undo
    updatetime = 250,                           -- faster completion (4000ms default)
    expandtab = true,                           -- convert tabs to spaces
    shiftwidth = 4,                             -- the number of spaces inserted for each indentation
    tabstop = 4,                                -- insert 4 spaces for a tab
    number = true,                              -- set numbered lines
    relativenumber = true,                      -- set relative numbered lines
    numberwidth = 3,                            -- set number column width to 2 {default 4}
    signcolumn = "yes",                         -- always show the sign column, otherwise it would shift the text each time
    wrap = false,                               -- softwrap

    -- multi edit
    swapfile = false,
    autoread = true,

    -- Folding
    foldcolumn = '0',                           -- '0' is not bad
    foldlevel = 99,                             -- Using ufo provider need a large value, feel free to decrease the value
    foldlevelstart = 99,
    foldenable = true,

    -- Lua tree
    termguicolors = true,
}

for key, value in pairs(options) do
    vim.opt[key] = value
end
