local telescope = require('telescope.builtin')

require('telescope').setup {
    defaults = {
        layout_strategy = 'flex',
    },

    pickers = {
        find_files = {
            initial_mode = 'insert',
        },
        live_grep = {
            initial_mode = 'insert',
        },
        buffers = {
            initial_mode = 'normal',
        },
        lsp_references = {
            initial_mode = 'normal',
        },
        diagnostics = {
            initial_mode = 'normal',
        },
    },
}

vim.keymap.set('n', '<leader>ff', telescope.find_files, {})
vim.keymap.set('n', '<leader>fg', telescope.live_grep, {})
vim.keymap.set('n', '<leader>fb', telescope.buffers, {})
