local bufferline = require('bufferline')

-- Bufferline set-up
vim.opt.termguicolors = true

bufferline.setup {
    options = {
        mode = 'buffers',
    }
}

vim.keymap.set(
    'n',
    'gb',
    ':BufferLinePick<CR>',
    {})
