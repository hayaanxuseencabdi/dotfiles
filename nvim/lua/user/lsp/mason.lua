require('mason').setup({
    log_level = vim.log.levels.DEBUG,
})

require('mason-lspconfig').setup({
    ensure_installed = {
        'clangd',
        'lua_ls',
    }
})

local capabilities = require('cmp_nvim_lsp').default_capabilities()
local telescope = require('telescope.builtin')
local ufo = require('ufo')

local function on_attach()
    -- LSP
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, { buffer = true })
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, { buffer = true })
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, { buffer = true })
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, { buffer = true })
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, { buffer = true })
    vim.keymap.set('n', 'gr', telescope.lsp_references, { buffer = true })
    vim.keymap.set('n', 'gt', vim.lsp.buf.type_definition, { buffer = true })
    vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { buffer = true })

    vim.keymap.set('n', '<C-.>', vim.lsp.buf.code_action, { buffer = true })

    -- Folding (UFO)
    vim.keymap.set('n', 'zR', ufo.openAllFolds)
    vim.keymap.set('n', 'zM', ufo.closeAllFolds)
end

-- Folding
ufo.setup({
    provider_selector = function(bufnr, filetype, buftype)
        return { 'treesitter', 'indent' }
    end
})

require('lspconfig')['lua_ls'].setup {
    settings = {
        Lua = {
            diagnostics = { globals = { 'vim' } }
        }
    },
    capabilities = capabilities,
    on_attach = on_attach,
}


require('lspconfig')['clangd'].setup {
    cmd = {
        'clangd',
        '--log=info',
        '--completion-style=bundled',
        '--pretty',
        '--clang-tidy',
        -- '--query-driver=/usr/local/bin/g++',
    },
    init_options = {
        compilationDatabasePath = "build",
    },
    capabilities = capabilities,
    on_attach = on_attach,
}

require('lspconfig')['pylsp'].setup {
    capabilities = capabilities,
    on_attach = on_attach,
}

require('lspconfig')['rust_analyzer'].setup {
    capabilities = capabilities,
    on_attach = on_attach,
}

require('lspconfig')['dockerls'].setup {
    capabilities = capabilities,
    on_attach = on_attach,
}

require('lspconfig')['cmake'].setup {
    capabilities = capabilities,
    on_attach = on_attach,
}

require('lspconfig')['jsonls'].setup {
    capabilities = capabilities,
    on_attach = on_attach,
}

require 'nvim-treesitter.configs'.setup {
    disable = {
        "cmake",
    },
    ensure_installed = {
        'bash',
        'c',
        'cpp',
        'lua',
        'json',
        'gitcommit',
        'python',
    },
    highlight = {
        enable = true,
    },
}
