alias xopen="xdg-open"

# Shorthand for dnf install with weak deps disabled.
alias dnfi="sudo dnf install --setopt=install_weak_deps=False"

# Activate a virtual environment.
alias activate="source venv/bin/activate.fish"

# For signing commits.
set -x GPG_TTY (tty)

# Useful mkdir $argv and cd $argv utility function
function mkcd
        mkdir -pv $argv;
        cd $argv;
end

# Read objtracer log file
function objtracer
        cut -f 2 -d ] $argv;
end
